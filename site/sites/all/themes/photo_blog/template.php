<?php
	function photo_blog_preprocess_page(&$varsiables)
	{
		$page = $varsiables["page"];
		$varsiables["photo_blog"]["main-content-width"] = "three-fourths";
		
		if(empty($page["right_column"]))
		{
			//need to assign a class "full-width" to main content
			$varsiables["photo_blog"]["main-content-width"] = "full-width";
		}
	}
?>