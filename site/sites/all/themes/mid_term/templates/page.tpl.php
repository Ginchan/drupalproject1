    <div class="site-container no-bg no-border">
        <!-- site title -->
        <div class="site-title-container">
            <div class="site-title">
                <p>Your new theme</p>
            </div>
        </div>
    </div>
    
    <div class="site-container">
        
        <div class="content-container container">
            <div class="menu-container">
                <ul class="menu">
					<?php 
						print theme('links',array('links'=>$main_menu));
					?>
                </ul>
            </div>
            
            <div class="content clearfix">
                
                <!-- messages will go here -->
                <div id="messages">
					<?php
						if($page['messages']):
							print render($page['messages']);
						endif; 
					?>	
                </div>
                
                <!-- tabs will go here -->
                <div class="tab-container container">
					<?php if ($tabs): ?>
            
						<div class="tabs">
							<?php print render($tabs); ?>
              			</div>
			
            		<?php endif; ?>
                </div>
                
                <!-- left column region -->
                <div class="left-column column region one-fourth left">
                    
                    <div class="inner">
						<?php
							if($page['left_column']):
								print render($page['left_column']);
							endif; 
						?>
                    </div>
                </div>
                
                <div class="main-content three-fourths left">
                    
                    <!-- main content -->
                    <div class="title">
						<?php
							if($page['title']):
								print render($page['title']);
							endif;
						?>
                    </div>
                    
                    <div class="content">
						<?php print render($page['content']); ?>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- footer region -->
        <div class="footer-container container">
            <div class="footer-content inner-container">
				<?php
					if($page['footer']):
						print render($page['footer']);
					endif; 
				?>	
            </div>
        </div>
        
    </div>